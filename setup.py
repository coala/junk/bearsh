from setuptools import setup


setup(
    name='bearsh',
    description="IPython shell extension for coala",
    author="Stefan Zimmermann",
    author_email="user@zimmermann.co",

    license='AGPLv3',

    setup_requires=['setuptools_scm'],
    use_scm_version={'local_scheme': lambda _: ''},

    install_requires=['coala-bears ~= 0.11'],

    packages=['bearsh'],

    classifiers=[
        'Development Status :: 4 - Beta',

        'Intended Audience :: Developers',

        'License :: OSI Approved :: GNU Affero General Public License '
        'v3 or later (AGPLv3+)',

        'Operating System :: OS Independent',

        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3 :: Only',

        'Topic :: Software Development :: Quality Assurance',
    ],
    keywords=[
        'coala', 'analyzer', 'analysis', 'shell', 'bear', 'bears',
        'ipython', 'magic', 'debug', 'debugging', 'pdb', 'ipdb',
    ],
)
