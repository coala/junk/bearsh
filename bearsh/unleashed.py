"""
Unleash the Bears!.

An interface for running Bears without an active coala process.
"""

from functools import lru_cache

from path import Path

from coalib.collecting.Collectors import get_all_bears, get_all_bears_names
from coalib.settings.Section import Section


def run_bear(bearcls, instance, input, **options):
    """
    Analyze `input` with :class:`Unleashed` Bear `instance`.

    :param bearcls:
       The original coala Bear class.
    :param input:
       Either a file ``path.Path`` instance or a ``str`` of input data.
    """
    if isinstance(input, Path):
        filename = input
        data = input.lines()
    else:
        filename = ':bearsh-input:'
        data = [line + '\n' for line in str(input).split('\n')]
    return bearcls.run(instance, filename, data, **options)


class UnleashedMeta(type):

    @lru_cache()
    def __getitem__(cls, bear):
        """
        Create :class:`Unleashed` wrapper class for coala `bear` class.
        """
        class Meta(type(cls), type(bear)):
            pass

        class UnleashedBear(cls, bear, metaclass=Meta):
            __module__ = __name__

            def run(self, input, **options):
                return run_bear(bear, self, input, **options)

        UnleashedBear.__name__ = '{}[{}]'.format(
            cls.__name__, bear.__name__)
        UnleashedBear.__qualname__ = '{}[{}]'.format(
            cls.__qualname__, bear.__qualname__)
        return UnleashedBear

    @lru_cache()
    def __getattr__(cls, bearname):
        for bear in get_all_bears():
            if bear.__name__ == bearname:
                return cls[bear]

    @lru_cache()
    def __dir__(cls):
        return super().__dir__() + get_all_bears_names()


class Unleashed(metaclass=UnleashedMeta):

    def __init__(self):
        pass
        # section = Section(':bearsh:')
        # self.bear = type(self).bear(section, message_queue=None)
