"""
Define functions for loading bearsh into IPython.

:func:`load` does the actual magic.

:func:`load_ipython_extension` is implicitly called by IPython on
``%load_ext bearsh``.
"""

from path import Path

from .unleashed import Unleashed

__all__ = ('load', 'Unleashed')


def load(shell):
    """
    Register all availabe coala Bears as IPython magic functions.

    A magic line Bear takes a space-seperated list of file patterns::

       %PEP8Bear **.py ...

    A magic cell Bear takes source code to analyze::

       %%PEP8Bear
       class Source:
           ...
    """
    from coalib.collecting.Collectors import get_all_bears

    for bear in get_all_bears():
        name = bear.name

        def line_magic(arg_str, _bearname=name):
            """
            Run the {bearname} on the given space-separated file patterns.

            ::
               In [*]: %{bearname} **.py
               Executing section Default...

            {beardoc}
            """
            instance = getattr(Unleashed, _bearname)()
            return [result for file in arg_str.split()
                    for result in instance.run(Path(file)) or ()]

        line_magic.__doc__ = line_magic.__doc__.format(
            bearname=name, beardoc=bear.get_metadata().desc)
        shell.magics_manager.magics['line'][name] = line_magic

        def cell_magic(arg_str, cell_str, _bearname=name):
            """
            Run the {bearname} on the given content.

            ::
               In [*]: %%{bearname}
                  ...: class Source:
                  ...:     ...
               Executing section Default...

            {beardoc}
            """
            instance = getattr(Unleashed, _bearname)()
            return [result for result in instance.run(cell_str) or ()]

        cell_magic.__doc__ = cell_magic.__doc__.format(
            bearname=name, beardoc=bear.get_metadata().desc)
        shell.magics_manager.magics['cell'][name] = cell_magic


def load_ipython_extension(shell):
    """
    Called by IPython on ``%load_ext bearsh``.

    Calls :func:`bearsh.load`, which does the actual magic.
    """
    load(shell)
