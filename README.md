# bearsh

This is a bear shell that helps you developing coala bears and lets you run them right in iPython!

bearsh is always written with a lower case b. Yes. Even at the beginning of a sentence. Spot a pattern?

# Usage

Fire up iPython, load the extension and you'll be able to load your bear:

```
$ ipython
Python 3.5.2 (default, Nov  7 2016, 11:31:36) 
Type "copyright", "credits" or "license" for more information.

IPython 5.1.0 -- An enhanced Interactive Python.
?         -> Introduction and overview of IPython's features.
%quickref -> Quick reference.
help      -> Python's own help system.
object?   -> Details about 'object', use 'object??' for extra details.

In [1]: %load_ext bearsh

In [2]: # Run the PEP8Bear on setup.py

In [3]: %PEP8Bear setup.py
Executing section Default...

In [4]:
```
